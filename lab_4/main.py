import re
import keras
from string import punctuation
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import *
import tensorflow
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from keras.layers import Dense


def str_to_category(str_list):
    categories = list()
    res = list()
    for el in str_list:
        if el not in categories:
            categories.append(el)
        res.append(categories.index(el))
    return res


def clean_messages(messages):
    re_puncs = re.compile('[%s]' % re.escape(punctuation))
    stop_words  = set(stopwords.words('english'))
    lem = WordNetLemmatizer()
    p_stem = PorterStemmer()
    res = list()
    for message in messages:
        tokens = word_tokenize(message)
        tokens = [w.lower() for w in tokens]
        tokens = [re_puncs.sub('', w) for w in tokens]
        tokens = [i for i in tokens if i.isalpha()]
        tokens = [w for w in tokens if w not in stop_words]
        tokens = [lem.lemmatize(w) for w in tokens]
        tokens = [p_stem.stem(w) for w in tokens]
        res.append(' '.join(tokens))
    # messages = [re_puncs.sub('', w.lower()) for w in messages]
    # messages = [w for w in messages if w.isalpha() and w not in set(stopwords.get_stopwords('english'))]
    return res


if __name__ == "__main__":
    train = np.genfromtxt('train.txt', delimiter=',', encoding='utf8', dtype=None)
    test = np.genfromtxt('test.txt', delimiter=',', encoding='utf8', dtype=None)
    train = np.transpose(train)
    test = np.transpose(test)

    [themes], [sentiments], [messages] = np.split(train, 3, 0)
    messages = clean_messages(messages)
    # themes_train = tensorflow.keras.utils.to_categorical(str_to_category(themes), len(set(themes)))
    sentiments_train = tensorflow.keras.utils.to_categorical(str_to_category(sentiments), len(set(sentiments)))

    vectorizer = TfidfVectorizer()
    train_vec = vectorizer.fit_transform(messages).toarray()

    # model = keras.Sequential()
    # model.add(Dense(500, input_shape=(len(train_vec[0]),), activation='sigmoid'))
    # model.add(Dense(len(set(sentiments)), activation='softmax'))
    # model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model = keras.Sequential([
        Dense(1024, input_dim=len(train_vec[0]), activation='relu'),
        Dense(512, activation='relu'),
        Dense(256, activation='relu'),
        Dense(128, activation='relu'),
        Dense(64, activation='relu'),
        Dense(len(set(sentiments)), activation='softmax')
    ])
    model.compile(
        loss='categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy']
    )

    model.fit(train_vec, sentiments_train, epochs=20, validation_split=0.1)

    [themes], [sentiments], [messages] = np.split(test, 3, 0)
    messages = clean_messages(messages)
    # messages = [re.sub('[,.:;`?!<>()\\[\\]{}@]', '', line).lower() for line in messages]

    # themes_test = tensorflow.keras.utils.to_categorical(str_to_category(themes), len(set(themes)))
    sentiments_test = tensorflow.keras.utils.to_categorical(str_to_category(sentiments), len(set(sentiments)))

    test_vec = vectorizer.transform(messages).toarray()

    score = model.evaluate(test_vec, sentiments_test)
    print(score)
