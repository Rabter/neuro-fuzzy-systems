from math import sqrt

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, DBSCAN


def euclid(a, b):
    s = 0
    for i in range(len(a)):
        s += pow(a[i] - b[i], 2)
    return sqrt(s)


def elbow(points, labels):
    clusters = np.array([points[labels == i] for i in set(labels)], dtype=object)
    centroids = np.array([cluster.mean(axis=0) for cluster in clusters])
    res = 0
    for i in range(len(clusters)):
        s = 0
        for point in clusters[i]:
            s += euclid(point, centroids[i]) ** 2
        res += s
    return res / len(clusters)


def DBI(points, labels):
    def cluster_density(c, points):
        dist = 0
        for point in points:
            dist += euclid(c, point)
        return dist / len(points)

    max = list()
    clusters = np.array([points[labels == i] for i in set(labels)], dtype=object)
    centroids = np.array([cluster.mean(axis=0) for cluster in clusters])
    for i in range(len(centroids)):
        max.append(-1)
        for j in range(len(centroids)):
            if i != j:
                density = cluster_density(centroids[i], clusters[i]) + cluster_density(centroids[j], clusters[j])
                centroids_dist = euclid(centroids[i], centroids[j])
                if centroids_dist == 0:
                    return 0
                tmp = density / centroids_dist
                if tmp > max[i]:
                    max[i] = tmp
    return np.mean(max)


def silhouette(points, labels):
    def compactness(point, cluster):
        s = 0
        for point1 in cluster:
            s += euclid(point, point1)
        return s / len(cluster)

    def separability(point, ci, clusters):
        res = list()
        for i in range(len(clusters)):
            if i != ci:
                res.append(compactness(point, clusters[i]))
                res.sort()
                res = res[:1]
        return res[0]

    clusters = np.array([points[labels == i] for i in set(labels)], dtype=object)

    s = 0
    for i in range(len(clusters)):
        for point in clusters[i]:
            sep = separability(point, i, clusters)
            comp = compactness(point, clusters[i])
            s += (sep - comp) / max(sep, comp)

    return s / len(points)


def neighbours_distances(points, n_neighbours):
    res = list()
    for i in range(len(points)):
        dists = list()
        for j in range(len(points)):
            if i != j:
                dists.append(euclid(points[i], points[j]))
        dists.sort()
        dists = dists[:n_neighbours]
        res.append(np.mean(dists))
    res.sort()
    return res


def kmeans(X, k):
    kmeans = KMeans(n_clusters=k)
    return kmeans.fit_predict(X)


data = np.genfromtxt('water.csv', delimiter=',')

DBI_res = list()
elbow_res = list()
silhouette_res = list()
max_clusters = 15
x = range(2, max_clusters + 1)
for k in x:
    res = kmeans(data, k)
    DBI_res.append(DBI(data, res))
    elbow_res.append(elbow(data, res))
    silhouette_res.append(silhouette(data, res))
    print("\b\b\b\b\b\b%2.2f" % (100 * k / max_clusters) + '%', end='')
print()

plt.figure(1)
plt.title("Davies–Bouldin Index")
plt.plot(x, DBI_res)
plt.xticks(x)
plt.figure(2)
plt.title("Elbow Method")
plt.plot(x, elbow_res)
plt.xticks(x)
plt.figure(3)
plt.title("Silhouette")
plt.plot(x, silhouette_res)
plt.xticks(x)

n = 5
dists = neighbours_distances(data, n)
plt.figure(4)
plt.title("Epsilon")
plt.plot(dists)
plt.show()
