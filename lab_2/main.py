import numpy as np
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier


def confusion_matrix(real, predicted, threshold=0.5):
    res = np.zeros((2, 2))
    for i in range(len(real)):
        if real[i] > threshold:
            col = 1
        else:
            col = 0
        if predicted[i] > threshold:
            line = 1
        else:
            line = 0
        res[line][col] += 1
    return res


def print_stats(real, predicted):
    print("Confusion matrix:")
    mat = confusion_matrix(real, predicted)
    print(mat)
    recall = mat[0][0] / (mat[0][0] + mat[1][0])
    print("Recall:", recall)
    precision = mat[0][0] / (mat[0][0] + mat[0][1])
    print("Precision:", precision)
    print("Accuracy:", (mat[0][0] + mat[1][1]) / (len(real)))
    print("F-measure:", 2 * recall * precision / (recall + precision))


data = np.genfromtxt('water.csv', delimiter=',')

train_ratio = int(len(data) * 0.8)
X_train = data[:train_ratio, :-1]
X_test = data[train_ratio:, :-1]
Y_train = data[:train_ratio, -1]
Y_test = data[train_ratio:, -1]

scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

pca = PCA(n_components=2)

pca.fit(X_train, Y_train)
X_train = pca.transform(X_train)

neighbors = KNeighborsClassifier()
neighbors.fit(X_train, Y_train)
decision_tree = DecisionTreeClassifier()
decision_tree.fit(X_train, Y_train)

X_test = pca.transform(X_test)
neighbors_res = neighbors.predict(X_test)
dt_res = decision_tree.predict(X_test)
#neighbors_res = 1 - neighbors.predict(X_test)
#dt_res = 1 - decision_tree.predict(X_test)

print("K Neighbors:")
print_stats(Y_test, neighbors_res)

print("\nDecision Tree:")
print_stats(Y_test, dt_res)

plt.figure(1)
plt.title("Real classes")
plt.scatter(X_test[:, 0], X_test[:, 1], c=Y_test, cmap='rainbow')

plt.figure(2)
plt.title("K Neighbors classification")
plt.scatter(X_test[:, 0], X_test[:, 1], c=neighbors_res, cmap='rainbow')

plt.figure(3)
plt.title("Decision Tree classification")
plt.scatter(X_test[:, 0], X_test[:, 1], c=dt_res, cmap='rainbow')

plt.show()
