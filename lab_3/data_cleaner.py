import re


def prepare_data(fin_name, fout_name):
    with open(fin_name, 'r', encoding="utf-8") as fin:
        saved_ids = set()
        res = list()
        i = 0
        for line in fin:
            if re.fullmatch("\\d*,.*,(Positive|Negative|Neutral|Irrelevant),.*\n", line) is None:
                res[i - 1] = res[i - 1].rstrip('\n') + line.replace(',', '')
            else:
                id = line[:line.index(',')]
                if id not in saved_ids:
                    saved_ids.add(id)
                    line = line.split(',', 3)
                    res.append(line[1] + ',' + line[2].replace() + ',' + line[3].replace(',', ''))
                    i += 1

    with open(fout_name, 'w', encoding="utf-8") as fout:
        fout.writelines(res)


if __name__ == "__main__":
    prepare_data("twitter_training.csv", "train.txt")
    prepare_data("twitter_validation.csv", "test.txt")
