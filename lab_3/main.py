import numpy as np
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier


def confusion_matrix(real, predicted, target):
    res = np.zeros((2, 2))
    for i in range(len(real)):
        if real[i] == target:
            col = 1
        else:
            col = 0
        if predicted[i] == target:
            line = 1
        else:
            line = 0
        res[line][col] += 1
    return res


def print_stats(real, predicted):
    print("Confusion matrix:")
    print('Enter target class (one of ', set(real), '):', sep='')
    target = input()
    mat = confusion_matrix(real, predicted, target)
    print(mat)
    recall = mat[0][0] / (mat[0][0] + mat[1][0])
    print("Recall:", recall)
    precision = mat[0][0] / (mat[0][0] + mat[0][1])
    print("Precision:", precision)
    print("Accuracy:", (mat[0][0] + mat[1][1]) / (len(real)))
    print("F-measure:", 2 * recall * precision / (recall + precision))


train = np.genfromtxt('train.txt', delimiter=',', encoding='utf8', dtype=None)
test = np.genfromtxt('test.txt', delimiter=',', encoding='utf8', dtype=None)
train = np.transpose(train)
test = np.transpose(test)

[themes], [sentiments], [messages] = np.split(train, 3, 0)
messages = [re.sub('[,@]', '', line) for line in messages]

vectorizer = TfidfVectorizer()
train_vec = vectorizer.fit_transform(messages)

neighbors_sentiments = KNeighborsClassifier()
neighbors_sentiments.fit(train_vec.toarray(), sentiments)
neighbors_themes = KNeighborsClassifier()
neighbors_themes.fit(train_vec.toarray(), themes)

[themes], [sentiments], [messages] = np.split(test, 3, 0)
messages = [re.sub('[,@]', '', line) for line in messages]

test_vec = vectorizer.transform(messages)

sentiments_res = neighbors_sentiments.predict(test_vec.toarray())
themes_res = neighbors_themes.predict(test_vec.toarray())

print("Sentiment analysis:")
print_stats(sentiments, sentiments_res)
print("\nTheme analysis:")
print_stats(themes, themes_res)
